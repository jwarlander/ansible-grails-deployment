# Ansible Grails Deployment

This is a simple example of an Ansible playbook that will deploy
a [Grails](https://grails.org/) project in Tomcat 7 on an Ubuntu
14.04 server.

Since I know next to nothing about Grails, I've just picked the
[grails-petclinic](https://github.com/grails-samples/grails-petclinic)
sample project.

The playbook itself is fairly naive, just doing the simplest thing
possible. If it were a real project, you'd want to set up Apache
or Nginx in front of Tomcat, configure a local firewall to block
unwanted traffic, set up a real database like PostgreSQL, etc.

Also, you'd want to restructure the playbook to make use of roles,
variables, perhaps include other playbooks, use the Ansible Vault
for encrypted storage of database passwords, etc.

## Setup

Install Ansible:

    $ sudo apt-get install software-properties-common
    $ sudo apt-add-repository ppa:ansible/ansible
    $ sudo apt-get update
    $ sudo apt-get install ansible

Make sure you have an SSH key in ~/.ssh/id_rsa:

    $ mkdir -p ~/.ssh && chmod 700 ~/.ssh
    $ ssh-keygen -t rsa

Create a VPS; [Digital Ocean](https://www.digitalocean.com/?refcode=86e0a97ea147)
isn't a bad choice, and if you use the provided link you'll get $10 in credit.

-   Make sure to add the SSH key from above, so that you can log
    in as `root` over the network

-   Select an image with at least 1GB or so of RAM; 512MB is too
    little to build Grails and the sample project, it seems

Once you have the IP number of your brand new server, create a
`hosts` file here with the following content, replacing `1.2.3.4`
with the actual IP number:

    [webservers]
    1.2.3.4

## Deploying

This is the easy part:

    ansible-playbook -i hosts -vv site.yml

If you get a timeout, or if you're disconnected before the
deployment is finished, just run it again; it'll skip any steps
it can, and pick up where it left off.
